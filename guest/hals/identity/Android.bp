//
// Copyright (C) 2022 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package {
    default_applicable_licenses: ["Android-Apache-2.0"],
}

cc_binary {
    name: "android.hardware.identity-service.remote",
    relative_install_path: "hw",
    vendor: true,
    cflags: [
        "-Wall",
        "-Wextra",
        "-g",
    ],
    stl: "c++_static",
    shared_libs: [
        "libbinder_ndk",
        "libcrypto",
        "liblog",
    ],
    static_libs: [
        "android.hardware.identity-V3-ndk",
        "android.hardware.identity-support-lib",
        "android.hardware.keymaster-V3-ndk",
        "android.hardware.security.keymint-V1-ndk",
        "libbase",
        "libcppbor",
        "libcppcose_rkp",
        "libkeymaster_portable",
        "libpuresoftkeymasterdevice",
        "libsoft_attestation_cert",
        "libsoft_attestation_cert",
        "libutils",
    ],
    local_include_dirs: [
        "common",
        "libeic",
    ],
    srcs: [
        "RemoteSecureHardwareProxy.cpp",
        "common/IdentityCredential.cpp",
        "common/IdentityCredentialStore.cpp",
        "common/WritableIdentityCredential.cpp",
        "libeic/EicCbor.c",
        "libeic/EicOpsImpl.cc",
        "libeic/EicPresentation.c",
        "libeic/EicProvisioning.c",
        "service.cpp",
    ],
    installable: false, // installed in APEX
}

prebuilt_etc {
    name: "android.hardware.identity_credential.remote.xml",
    sub_dir: "permissions",
    vendor: true,
    src: "android.hardware.identity_credential.remote.xml",
    installable: false,
}

prebuilt_etc {
    name: "android.hardware.identity-service.remote.xml",
    src: "android.hardware.identity-service.remote.xml",
    sub_dir: "vintf",
    installable: false,
}

prebuilt_etc {
    name: "android.hardware.identity-service.remote.rc",
    src: "android.hardware.identity-service.remote.rc",
    installable: false,
}

apex {
    name: "com.google.cf.identity",
    vendor: true,
    manifest: "apex_manifest.json",
    file_contexts: "apex_file_contexts",
    key: "com.google.cf.apex.key",
    certificate: ":com.google.cf.apex.certificate",
    updatable: false,

    binaries: ["android.hardware.identity-service.remote"],
    prebuilts: [
        "android.hardware.identity-service.remote.rc",
        "android.hardware.identity-service.remote.xml",
        "android.hardware.identity_credential.remote.xml",
    ],
}
